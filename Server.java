
//package sapxeptangdan;

/**
 *
 * @author lehuu
 */
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lehuu
 */
public class Server {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        new Server().run();
    }
    
    private void run()
    {
        
        int port = 6394;
        try {
            SapXepMang tt = new SapXepMang();
            Registry registry = LocateRegistry.createRegistry(port);
            Naming.rebind("rmi://localhost:"+port+"/SapXepMang", tt);
            System.out.println("Server dang hoat dong...");
        } catch (RemoteException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

