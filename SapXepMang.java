
//package sapxeptangdan;

/**
 *
 * @author lehuu
 */
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author lehuu
 */

public class SapXepMang extends UnicastRemoteObject implements SapXep 
{
   // private int A[];
   // private int n;
    
    public SapXepMang() throws RemoteException {
    }
    
    @Override
    public int[] sapXep(int A[],int n) throws RemoteException 
    {
        int tg;
        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                if(A[i]>A[j])
                {
                    tg=A[i];
                    A[i]=A[j];
                    A[j]=tg;
                }
            }
        } 
        return A;
    }
}