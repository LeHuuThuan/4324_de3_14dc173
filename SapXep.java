
//package sapxeptangdan;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 *
 * @author lehuu
 */
public interface SapXep extends Remote
{
    public int[] sapXep(int A[],int n) throws RemoteException;
}