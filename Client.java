
//package sapxeptangdan;

/**
 *
 * @author lehuu
 */

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;



public class Client {

    
    
    private int A[];
    private int n=0;
    
    public static void main(String[] args) throws RemoteException {
        new Client().run();
    }
    private void run() throws RemoteException
    {
        int port = 6394;
        try 
        {
            SapXep SX = (SapXep) Naming.lookup("rmi://localhost:"+port+"/SapXepMang");        
            Scanner nhapMang= new Scanner(System.in);
           
           
            System.out.print("Nhap kich thuoc mang can sap xep:  ");
            n =  nhapMang.nextInt();
            A = new int[n];
            
            for(int i=0;i<n;i++)
            {
                System.out.printf("Phan tu thu %d: ", i+1);
                A[i]=nhapMang.nextInt();
            }            
            System.out.print("Mang vua nhap la: ");
            for(int i = 0; i < A.length; i++)
                System.out.print(A[i]+"  ");
			System.out.println("\n");
            System.out.print("Mang sau khi sap xep: ");
            A = SX.sapXep(A, n);
            for(int i = 0; i < A.length; i++)
                System.out.print(A[i]+"   ");
            System.out.println("");
        } 
        catch (NotBoundException ex) 
        {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (MalformedURLException ex) 
        {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (RemoteException ex) 
        {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}